<?
	class Logger {

		private $handle;
		private $filename;
		private static $instance = NULL;

		private function __construct(&$filename) {
			$this->filename = $filename;
		}

		public static function init(&$filename) {
			if (is_null(self::$instance)) {
				self::$instance = new Logger($filename);
			}
			self::open();
		}

		private static function getCaller() {
			$traces = debug_backtrace();
			return $traces[2];
		}

		public static function getInstance() {
			if (is_null(self::$instance)) {
				$traces = self::getCaller();
				$function = $traces['class'] . $traces['type'] . $traces['function'];

				throw new Exception("Please use Logger::init function before calling {$function}");
			}
			return self::$instance;
		}

		public static function open() {
			$instance = self::getInstance();
			if (file_exists($instance->filename) && !is_writable($instance->filename)) {
				throw new Exception("The file {$instance->filename} is not writable");
			}
			if (! ($instance->handle = fopen($instance->filename, 'a')) ) {
				throw new Exception("Impossible to open file {$instance->filename}");
			}
		}

		private function write($message) {
			if (!fwrite($this->handle, $message)) {
				throw new Exception("Impossible to write in file {$this->filename} the message: {$message}");
			}
		}

		private function setTimestamp() {
			$instance = self::getInstance();
			$traces = self::getCaller();
			$timestamp = date_format(new DateTime(), 'Y-m-d H:i:s');
			$instance->write("[$timestamp][".strtoupper($traces['function'])."] ");
		}
		private function flush() {
			$instance = self::getInstance();
			$instance->write("\n");
		}

		public static function enter($message) {
			$instance = self::getInstance();
			$instance->setTimestamp();
			$instance->write($message);
			$instance->flush();
		}
		public static function leave($message) {
			$instance = self::getInstance();
			$instance->setTimestamp();
			$instance->write($message);
			$instance->flush();
		}

		public static function notice($message) {
			$instance = self::getInstance();
			$instance->setTimestamp();
			$instance->write($message);
			$instance->flush();
		}

		public static function debug($messages) {
			$instance = self::getInstance();

			$instance->setTimestamp();
			$instance->flush();
			if (!is_array($messages)) {
				$messages = array($messages);
			}
			foreach ($messages as $key => $message) {
				$data = "$key = ".json_encode($message). "\n";
				$instance->write($data);
			}
		}

		public static function error($message) {
			try {
				$instance = self::getInstance();
				$instance->setTimestamp();
				$message = json_encode($message);
				$instance->write($message);
			} catch (Exception $e) {

			}
		}

		public static function close() {
			try {
				$instance = self::getInstance();
				if ($instance->handle) {
					fclose($instance->handle);
					self::$instance = NULL;
				}
			} catch (Exception $e) {

			}
		}
	}
