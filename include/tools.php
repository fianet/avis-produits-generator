<?
	require_once ('database/Database.class.php');
	require_once ('logger/Logger.class.php');
	require_once ('FianetException.class.php');
	require_once ('defs.inc.php');

	function connectDb() {
		$db = new Database;
		$db->connect (DB_DSQUERY, DB_USER, DB_PASSWD, DB_DATABASE, DB_CHARSET, FALSE);
		return $db;
	}

	function doRequest(&$query) {
		try {

			$db = connectDb();

			$db->beginTran();

			$response = $db->exec($query);

			$data = array();
			while ( ($row = $response->assocfetch()) != NULL ) {
				$data[] = $row;
			}

			$db->commitTran();
			$db->close();

			return $data;

		} catch (DatabaseException $e) {
			var_export($e);
			$db->rollbackTran();
			throw $e;

		} catch (Exception $e) {
			var_export($e);
			$db->rollbackTran();
			throw $e;
		}

	}

	function cipher_md5($data) {
		if (function_exists('fianet_public_md5')) {
			return fianet_public_md5($data);
		} else {
			return md5($data);
		}
	}

	
	if (! function_exists('array_column')) {
		function array_column(array $input, $columnKey, $indexKey = null) {
			$array = array();
			foreach ($input as $value) {
				if ( !array_key_exists($columnKey, $value)) {
					trigger_error("Key \"$columnKey\" does not exist in array");
					return false;
				}
				if (is_null($indexKey)) {
					$array[] = $value[$columnKey];
				}
				else {
					if ( !array_key_exists($indexKey, $value)) {
						trigger_error("Key \"$indexKey\" does not exist in array");
						return false;
					}
					if ( ! is_scalar($value[$indexKey])) {
						trigger_error("Key \"$indexKey\" does not contain scalar value");
						return false;
					}
					$array[$value[$indexKey]] = $value[$columnKey];
				}
			}
			return $array;
		}
	}
	
