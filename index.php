<?

	require_once ('include/tools.php');

	define('JSON_DIR', '/home/slabel/htdocs/avis-produits/json');

	function my_getopt(&$args, &$options = array()) {
		//remove the name of the executing script
		array_shift($args);

		//no options chosen
		if (!is_array($args)) {
			usage();
		}

		//parse parameters
		while (($param = array_shift($args)) !== NULL) {
			switch ($param) {
				case '--sid':
				case '-s': {
					$options['sid'] = array_shift($args);
					break;
				}
				case '--pcid':
				case '-p': {
					$options['pcid'] = array_shift($args);
					break;
				}
				case '--log':
				case '-l': {
					$options['log'] = array_shift($args);
					break;
				}
				case '--debug':
				case '-d': {
					$options['debug'] = TRUE;
					break;
				}
				case '--help':
				case '-h': {
					usage();
					break;
				}
				default: {
					usage();
					break;
				}
			}
		}
	}

	function avp_init(&$argv, &$options = array()) {
		// global $_POST;
		if (php_sapi_name() == 'cli') {
			my_getopt($argv, $options);
		} else {
			if (isset($_POST['sid']) && !empty($_POST['sid'])) {
				$options['sid'] = $_POST['sid'];
			}
			if (isset($_POST['pcid']) && !empty($_POST['pcid'])) {
				$options['pcid'] = $_POST['pcid'];
			}
		}
	}

	function getAuthorizedSites(&$sites, $siteid = null, $debug = false) {
		Logger::enter(__FUNCTION__);

		$query = "
			SELECT DISTINCT S.SiteID
			FROM
				/* Sites Actifs */
				fianet2..Site S
					JOIN rating..RatingSiteAccess RSA
						ON S.SiteID = RSA.SiteID
					JOIN fianet2..SiteGarantie SG
						ON S.SiteID = SG.SiteID AND GarantieID = 7
					JOIN fianet2..SiteInfo SI
						ON S.SiteID = SI.SiteID AND InfoID = 32 AND ValueListe != 1
					JOIN rating..RatingSite RS
						ON S.SiteID = RS.SiteID
					/* Souscription a l'option avis produits */
					JOIN fianet2..vSiteExtranetDroitAll SED
						ON SED.SiteID = S.SiteID AND SED.OptionID = 15
		";

		if (!empty($siteid)) {
			$query .= " WHERE S.SiteID = {$siteid} ";
		}

		$results = doRequest($query);
		$sites = array_column($results, 'SiteID');

		if ($debug) {
			$tmp = array(
				'sites' => 'Found ' .count($sites). ' sites.'
			);

			Logger::debug($tmp);
		}

		Logger::leave(__FUNCTION__);
	}

	function getAuthorizedSitesWithProducts(&$sites, $sid, &$products, $pcid = null, $debug = false) {
		Logger::enter(__FUNCTION__);

		getAuthorizedSites($sites, $sid, $debug);

		$siteStr = " IN (". implode(',', $sites).")";
		$query = "
			SELECT DISTINCT P.SiteID, P.ProduitClientID
			FROM fianet2..Site S
				JOIN rating..Produit P
					ON P.SiteID = S.SiteID
				JOIN rating..ProduitStatSite PSS
					ON PSS.ProduitClientID = P.ProduitClientID AND PSS.SiteID = P.SiteID
			WHERE S.SiteID {$siteStr}
		";

		if (!empty($pcid)) {
			$query .= " AND P.ProduitClientID = '{$pcid}' ";
		}

		$results = doRequest($query);
		if ($results) {
			$sites = array_unique(array_column($results, 'SiteID'));
			$products = array_map('prepareProduit', array_unique(array_column($results, 'ProduitClientID')));

		}
		if ($debug) {
			$tmp = array(
				'sites' => 'Found ' .count($sites). ' sites',
				'products' => 'Found ' .count($products). ' products',
			);

			Logger::debug($tmp);
		}

		Logger::leave(__FUNCTION__);
	}

	function prepareProduit($item) {
		return "'" . $item . "'";
	}

	function getAvisProduits(&$sites, &$products, &$avis, $debug = false) {
		Logger::enter(__FUNCTION__);

		$siteStr = " IN (". implode(',', $sites).")";
		$productStr = " IN (". implode(',', $products).")";

		$query = "
			SELECT S.SiteID, P.ProduitClientID, CONVERT(VARCHAR, PQ.ReponseDate, 103) AS ReponseDate,
				PQ.ReponseDate DateReponse,
				TR.Nom, TR.Prenom, PR.ProduitNote, PR.ProduitAvis
			FROM rating..Produit P
				JOIN rating..TransactionRating TR ON TR.CommerceID = P.CommerceID
				JOIN rating..ProduitRepondu PR ON PR.ProduitID = P.ProduitID
				JOIN rating..ProduitQuestionnaire PQ ON PQ.CommerceID = P.CommerceID
				JOIN fianet2..Site S ON S.SiteID = P.SiteID
				LEFT JOIN rating..RatingRepondu RR ON RR.CommerceID = P.CommerceID
			WHERE S.SiteID {$siteStr}
				AND P.ProduitClientID {$productStr}
				AND PQ.ReponseDate BETWEEN CONVERT(DATE, DATEADD(dd, -366, getdate()), 103) AND CONVERT(DATE, GETDATE(), 103)
			ORDER BY DateReponse DESC
		";

		$results = doRequest($query);

		if ($debug) {
			$tmp = array(
				'reviews' => 'Found ' .count($results). ' reviews'
			);

			Logger::debug($tmp);
		}

		foreach ($results as $result) {
			$sid = $result['SiteID'];
			$pcid = $result['ProduitClientID'];
			unset($result['SiteID']);
			unset($result['ProduitClientID']);
			unset($result['DateReponse']);

			$avis[$sid][$pcid]['Avis'][] = $result;

		}

		Logger::leave(__FUNCTION__);
	}

	function getAvisStats(&$sites, &$products, &$avis, $debug = false) {
		Logger::enter(__FUNCTION__);

		$query = "
			SELECT PSS.SiteID, PSS.ProduitClientID, PSS.NbrAvis, PSS.Moyenne
			FROM rating..ProduitStatSite PSS
		";
		if (!empty($sites) || !empty($products)) {
			$query .= " WHERE ";
		}
		if (!empty($sites)) {
			$query .= " PSS.SiteID IN (". implode(',', $sites).") AND ";
		}
		if (!empty($products)) {
			$query .= " PSS.ProduitClientID IN (". implode(',', $products).")";
		}

		$results = doRequest($query);

		if ($debug) {
			$tmp = array(
				'reviewStats' => 'Found ' .count($results). ' review stats'
			);

			Logger::debug($tmp);
		}

		foreach ($results as $result) {
			$sid = $result['SiteID'];
			$pcid = $result['ProduitClientID'];
			unset($result['SiteID']);
			unset($result['ProduitClientID']);
			foreach ($result as $key => $value) {
				$avis[$sid][$pcid][$key] = floatval($value);
			}
		}
		Logger::leave(__FUNCTION__);
	}

	function consolidAvis(&$avis) {
		Logger::enter(__FUNCTION__);
		array_walk_recursive($avis, 'encodeData');
		Logger::leave(__FUNCTION__);
	}

	function storeAvis(&$avis, $debug = false) {
		Logger::enter(__FUNCTION__);
		if (!file_exists(JSON_DIR)) {
			mkdir(JSON_DIR, 0777, true);
		}

		foreach ($avis as $sid => $products) {
			$siteDir = JSON_DIR .'/'. $sid;
			if (!file_exists($siteDir)) {
				mkdir($siteDir, 0777, true);
			}


			foreach ($products as $pcid => $product) {
				$filename = $siteDir .'/'. cipher_md5($pcid) .'.json';
				if (file_exists($filename)) {
					unlink($filename);
				}
				if ($debug) {
					$tmp = array(
						'filename' => $filename,
						'json' => $product
					);

					Logger::debug($tmp);
				}
				file_put_contents($filename, json_encode($product));
			}
		}
		Logger::leave(__FUNCTION__);

	}


	function usage () {
		echo "Usage:\n";
		echo "     -s, --sid site id to treat [optional]\n";
		echo "     -p, --pcid produit client id to treat [optional]\n";
		echo "     -l, --log path to log folder\n";
		echo "     -d, --debug debug mode \n";
		echo "     -h, --help display this help and exit";
		echo "\n";
		exit(2);
	}

	try {
		$error = null;
		$sid = null;
		$pcid = null;

		avp_init($argv, $options);

		foreach ($options as $key => $value) {
			$$key = $value;
		}

		$debug = !empty($debug);

		if ($debug) {
			error_reporting(E_ALL);
			ini_set('xdebug.var_display_max_depth', 5);
			ini_set('xdebug.var_display_max_children', 256);
			ini_set('xdebug.var_display_max_data', 1024);
		}

		if (empty($sid) && !empty($pcid)) {
			throw new Exception('Provide a SiteID to retrieve a ProduitClientID');
		}

		Logger::init($log);

		Logger::enter('Avis Produits Generator');
		if ($debug) {
			Logger::debug($options);
		}

		getAuthorizedSitesWithProducts($sites, $sid, $products, $pcid, $debug);

		getAvisProduits($sites, $products, $avis, $debug);

		getAvisStats($sites, $products, $avis, $debug);

		consolidAvis($avis);

		storeAvis($avis, $debug);

		Logger::leave('Avis Produits');

	} catch (DatabaseException $e) {
		$error = $e;
	} catch (Exception $e) {
		$error = $e;
	}

	if($error) {
		Logger::error($error);
	}
	Logger::close();
	$code = (!empty($error)) ? 1 : 0;
	exit($code);
